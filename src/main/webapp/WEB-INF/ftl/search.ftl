<html>
<head>
    <meta charset="utf-8" />
    <#if query??>
        <title>brainMultiplexer - ${query}</title>
    <#else>
        <title>brainMultiplexer</title>
    </#if>
    <link rel="SHORTCUT ICON" HREF="/favicon.png">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script src="js/jqcloud.js"></script>
    <script src="js/ajax-server-communication.js"></script>

    <link rel="stylesheet" href="css/jqcloud.css">
    <link rel="stylesheet" href="css/search-bar.css">
    <link rel="stylesheet" href="css/feedback-modal-window.css">

    <style type="text/css">

        .navbar-nav > li{
            margin-left:5px;
            margin-right:5px;
            vertical-align: middle;
        }
        .navbar-nav > ul{
            vertical-align: middle;
        }

        #one {
            height: 100%;
            width: 100%;
        }

        #img_logo {
            height: 30%;
            width: 70%;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-default">
    <div align="center"  class="container-fluid">
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="http://www.brain-multiplexer.com"><img src="img/brain.png"></a></li>
                <li><a href="https://www.facebook.com/brainmultiplexer" target='_blank'><img src="img/fb.png" height="27" width="27" alt="Facebook"></a></li>
                <li><a href="https://twitter.com/brainMultiplex" target="_blank"><img src="img/tweet.png" height="27" width="27" alt="Twitter"></a></li>
                <li><a href="#"><img src="img/googleplus.png" height="27" width="27" alt="Pinterest"></a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="#"  id="modal_open_feedback">Feedback</a></li>
                <li><a href="#" id="modal_open_about">About</a></li>
            </ul>
        </div>
    </div>
</nav>

<datalist id="languages">

</datalist>

<script type="text/javascript">
    // A $( document ).ready() block.
    $( document ).ready(function() {

        $("#searchbar").keyup(function (event) {
            if (event.keyCode == 13) {
                console.log("salam")
                $("#languages").hide();
                searchQuery(document.getElementById('searchbar').value)
            } else {
                if ((event.which <= 90 && event.which >= 48) || (event.which == 32)) {
                    var text = document.getElementById('searchbar').value
                    if (text.length > 2) {
                        console.log("salam")
                        autocomplete(text)
                    }
                }
            }
        });

        $("#searchbar").focus();

        <#if results??>
            <#if results == "1">
                <#if query??>
                    document.getElementById('searchbar').value = "${query}";
                </#if>

                <#if total?? >
                    $("#total").show();
                </#if>

                <#assign t = total?number>

                <#if t gt 10 >
                    $("#more").show();
                <#else>
                    $("#more").hide();
                </#if>
            <#elseif results == "0">
                $("#more").hide();
                $("#tags").html("");
                $("#suggest").html("");
                $("#resultsdiv").html("");
                $("#total").html("");

                <#if didyoumean??>
                    var val = "${didyoumean}";
                    console.log(val);
                    var suggest = "<p align='left'>Did you mean: <a href = '#' onclick='searchQuery(escape($(this).text()))'>" + val + "</a></p>";
                    $("#suggest").html(suggest);
                    $("#suggest").show();
                </#if>
            <#elseif results == "-1">
                var suggest = "<p align='left'>No results found</p><img src='img/no_results.png'>"
                $("#more").hide();
                $("#tags").html("");
                $("#resultsdiv").html("");
                $("#total").html("");
                $("#suggest").html(suggest);
            </#if>

        <#else>
            $("#more").hide();
            $("#suggest").html("");
            $("#total").hide();
            $("#resultsdiv").html("");
        </#if>
    });

    function searchQuery(text) {
        if (text == "") {
            return;
        }
        text = unescape(text);
        if(text.substring(0, "related topic with:".length) == "related topic with:") {
            return;
        }

        document.getElementById('searchbar').value = text;
        document.getElementById("searhForm").submit();
        //sendAjax(document.getElementById('searchbar').value)
    }

</script>

<div id="one">
    <table style ="width: 90%" valign="top" border=0 align="center" style="height:100%;width: 100%;">
        <tr class="spaceUnder">
            <td align="center" valign="top" style="width:70%">
                <table align="center" valign="top" style="width:100%;height:100%;">
                    <tr class="spaceUnder">
                        <td>
                            <div id="searchdiv">
                                <div id="search-form">
                                    <form method="get" action="/search" id="searhForm">
                                        <input type="text" list="languages" id="searchbar" name="query"/>
                                        <input class="lemoor-button" id="btn" type="button"
                                               onclick='searchQuery(document.getElementById("searchbar").value)' value="Search" />
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="suggest"></div><span style="float: right" id="total"><#if total??>Total results: ${total}</#if></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="resultsdiv" style="overflow:scroll; overflow:hidden; word-wrap: break-word;">
                                <ul>
                                    <#if docs??>
                                    <#list docs as doc>
                                        <li>
                                            <table class="table table-hover" onmouseover='$("#${doc.id}").toggle();'
                                                    onmouseout='$("#${doc.id}").toggle();'>
                                                <tr>
                                                    <td>
                                                        <#if doc.image == "default-icon">
                                                            <a target='_blank' href=""><img src='img/default_icon.png' height=50 width=50 style='border-radius: 5px;'></a>
                                                        <#else>
                                                            <a target='_blank' href=""><img src="${doc.image}" height=50 width=50 style='border-radius: 5px;'></a>
                                                        </#if>
                                                    </td>
                                                    <td>
                                                        <table>
                                                            <tr>
                                                                <td class='myTable'>
                                                                    <span align='left' style='display: inline'>
                                                                        <b>
                                                                            <a target='_blank' href="${doc.url}">${doc.title}</a>
                                                                        </b>
                                                                    </span>
                                                                    <span style='display:none;float:right' id=${doc.id}>
                                                                        <a target='_blank' href='javascript:fbShare("http://brain-multiplexer.com/redir?q=" + ${doc.id}, "Fb Share", "Facebook share popup", "${doc.image}", 520, 350)'><img src="img/fb-share.png"></a>
                                                                        <a target='_blank' href='javascript:twitterShare("http://brain-multiplexer.com/redir?q=" + ${doc.id}, "Twitter Share", "Twitter share popup", "http://goo.gl/dS52U", 520, 350)'><img src='img/twitter-share.png'></a>
                                                                        <i>
                                                                            <a href='#' onclick='getSimilarResults($(this).parent().parent().attr("id"), escape("${doc.title}"))'>similar</a>
                                                                        </i>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <h5 align='left' style='color: #4ea923;display: inline'>${doc.url}</h5>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <p align='left' style='display: inline'>${doc.paragraph}</p>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </li>
                                    </#list>
                                    </#if>
                            </ul>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" align="center">
                            <input class="more-button" id="more" type="button" value="More Results"
                                   onclick="moreResults(document.getElementById('searchbar').value)"/>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="center" valign="top" style="width:25%; height: 100%">
                <table>
                    <tr><td style='color: #4ea923;display: inline'><p><b>Today's suggestions: </b></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Holocaust</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Margaret Thatcher</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Ancient Greece</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Jupiter</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Islam</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Yemen</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Neil DeGrasse</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Stalin</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Yugoslavia</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">The Renaissance</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Siberia</a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Environment </a></p></td></tr>
                    <tr><td><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Saudi Arabia</a></p></td></tr>
                    <tr><td class="spaceUnder"><p><a href="#" style='color: #4ea923;display: inline' onclick="searchQuery(escape($(this).text()))">Newton</a></p></td></tr>
                    <tr><td><div id="tags"></div></td><tr>
                </table>
            </td>
        </tr>
        <tr><td><br><br><br><br><h5 align="center" style="color:">a softInspire project</h5></td></tr>
    </table>

</div>

<div id="modal_wrapper_feedback">
    <div id="modal_window_feedback">

        <div style="text-align: right;">
            <a id="modal_close_feedback" href="#">close <b>X</b></a>
        </div>

        <p>Complete the form below to send an email:</p>

        <form id="modal_feedback" accept-charset="UTF-8">
            <p><label>Your Name<strong>*</strong><br>
                <input type="text" autofocus required size="48" name="name" id="feedback_name" value=""></label></p>
            <p><label>Email Address<strong>*</strong><br>
                <input type="email" required title="Please enter a valid email address" size="48" id="feedback_email" name="email" value=""></label></p>
            <p><label>Subject<br>
                <input type="text" size="48" name="subject" id="feedback_subject" value=""></label></p>
            <p><label>Enquiry<strong>*</strong><br>
                <textarea required name="message" id="feedback_message" cols="48" rows="8"></textarea></label></p>
            <p><input type="button" class="feedback-button" name="feedbackForm" value="Send Message" onclick="sendFeedback()"></p>
        </form>
    </div>
</div>
<script type="text/javascript" src="js/feedback-modal-window.js"></script>

<div id="modal_wrapper_about">
    <div id="modal_window_about">

        <div style="text-align: right;">
            <a id="modal_close_about" href="#">close <b>X</b></a>
        </div>

        <img src="img/logo_big.png" id="img_logo">
    </div>
</div>
<script type="text/javascript" src="js/about-modal-window.js"></script>

</body>
</html>