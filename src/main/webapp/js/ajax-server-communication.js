
function autocomplete(search) {
    if(search == "") {
        return;
    }

    var find = ' ';
    var re = new RegExp(find, 'g');

    search = search.replace(re, '_');

    console.log(search);
    $.ajax({
        url: "/autocomplete",
        type: 'GET',
        dataType: 'json',
        data: "query=" + search,
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            populateDataList(data);
        },
        error:function(data,status,er) {
            alert("Status: "+status+" er:"+er);
        }
    });
}

function sendAjax(search) {

    // get inputs
    if (search == "") {
        return;
    }

    $.ajax({
        url: "/search",
        type: 'GET',
        dataType: 'json',
        data: "query=" + search,
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            populateResultsDiv(data, "search");
        },
        error:function(data,status,er) {
            alert("Status: "+status+" er:"+er);
        }
    });
}

function moreResults(search) {
    // get inputs
    if (search == "") {
        return;
    }

    $.ajax({
        url: "/more",
        type: 'GET',
        dataType: 'json',
        data: "query=" + search,
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            populateResultsDiv(data, "more");
        },
        error:function(data,status,er) {
            alert("Status: "+status+" er:"+er);
        }
    });
}

function getSimilarResults(md5, title) {
// get inputs
    if (md5 == "") {
        return;
    }

    if (title == "") {
        return;
    }

    title = unescape(title);
    $.ajax({
        url: "/similar",
        type: 'GET',
        dataType: 'json',
        data: {
            query: JSON.stringify(md5)
        },
        contentType: 'application/json',
        mimeType: 'application/json',

        success: function (data) {
            document.getElementById('searchbar').value = "related topic with: " + "\"" + title + "\"";
            populateResultsDiv(data, "similar");
        },
        error:function(data,status,er) {
            alert("Status: "+status+" er:"+er);
        }
    });
}

function sendFeedback() {
    name = $("#feedback_name").val();
    email = $("#feedback_email").val();
    subject = $("#feedback_subject").val();
    message = $("#feedback_message").val();

    $.ajax({
        url: "/feedback",
        type: 'post',
        data: "name=" + name + "&email=" + email + "&subject=" + subject + "&message=" + message,
        mimeType: 'application/json',
        success: function (data) {
        }
    });

    var modalWrapper = document.getElementById("modal_wrapper_feedback");
    modalWrapper.className = "";
    e.preventDefault ? e.preventDefault() : e.returnValue = false;
}

function suggestion(didyoumean) {
    $("#searchbar").val(didyoumean);
    sendAjax(document.getElementById("searchbar").value);
}

function populateResultsDiv(data, called) {
    var div = "";
    var suggest = "";
    var docs;

    do {

        if (called == "search") {
            if (data.documents.ignore == "1") {
                break;
            }

            if (data.documents.results == "1") {

                docs = data.documents.docs;
                if (docs.length <= 0) {
                    alert("There is a serious error");
                    break;
                }

                $("#total").html("");
                total = " Total results: " + data.documents.total;
                $("#total").html(total);

                div = constructResultsdiv(docs);
                tags = data.documents.tags;

                $("#suggest").html("");

                $("#tags").html("");

                if (tags.length > 0)
                    buildTagTable(tags);

                if(data.documents.total > 10)
                    $("#more").show();
                else
                    $("#more").hide();

                $("#resultsdiv").html(div);

            } else if (data.documents.results == "0") {
                $("#more").hide();
                $("#tags").html("");
                $("#suggest").html("");
                $("#resultsdiv").html("");
                $("#total").html("");

                didyoumean = data.documents.didyoumean;
                console.log(didyoumean);
                suggest += "<p align='left'>Did you mean: <a href = '#' onclick='suggestion(didyoumean)'>" + didyoumean + "</a></p>";
                $("#suggest").html(suggest);
            } else if (data.documents.results == "-1") {
                suggest += "<p align='left'>No results found</p><img src='img/no_results.png'>"
                $("#more").hide();
                $("#tags").html("");
                $("#resultsdiv").html("");
                $("#total").html("");
                $("#suggest").html(suggest);

            }
        } else if(called == "more") {
            docs = data.documents.docs;
            if (docs.length == 0) {
                break;
            }

            div = constructResultsdiv(docs);
            if(data.documents.final == "1") {
                $("#more").hide();
            } else {
                $("#more").show();
            }
            $("#resultsdiv").append(div);
        } else if (called == "similar") {
            $("#total").html("");
            $("#more").hide();
            $("#tags").html("");
            $("#suggest").html("");

            docs = data.documents.docs;
            if (docs.length <= 0) {
                alert("There is a serious error");
                break;
            }

            div = constructResultsdiv(docs);
            $("#resultsdiv").html(div);
        }
    } while(false);
}

function buildTagTable(tags) {

    var query = document.getElementById('searchbar').value;
    var tagsHtml = "<table><tr class='spaceUnder'><td style='color: #4ea923;display: inline'><p><b>Related Searches:</b></p></td><tr>";
    for(j = 0; j < tags.length; j++) {
        if (tags[j].text.toLocaleLowerCase() === query.toLocaleLowerCase()) {
            continue;
        }

        tagsHtml += "<tr><td><p><a href='#' style='color: #4ea923;display: inline' onclick='searchQuery(escape($(this).text()))'>" + tags[j].text + "</a></p></td></tr>"
    }
    tagsHtml += "</table>";
    $("#tags").html(tagsHtml);

}

function constructResultsdiv(docs) {
    var div = "<ul><li>"
    var first = -1;
    var current = -100;
    var end = 0;
    for (i = 0; i < docs.length; i++) {
        if (docs[i].series == "1") {
            if (first == -1) {
                if (i < docs.length - 2 && docs[i + 1].series == "1") {
                    current = i;
                    first = 1;
                    div += "<ul><li>";
                }
            } else {
                current = i;
            }

        } else if (current == i - 1) {
            current = -100;
            first = -1;
            div += "</ul><li>";
        }

        div += "<table class='table table-hover' " +
            "onmouseover='$(" + "\"#" + docs[i].id + "\"" + ").toggle();' " +
            "onmouseout='$(" + "\"#" + docs[i].id + "\"" + ").toggle();'><tr><td>";

        if (docs[i].image == "default-icon") {
            div += "<a target='_blank' href='" + docs[i].url + "'>";
            div += "<img src='img/default_icon.png' height=50 width=50 style='border-radius: 5px;'></a></td><td>";
        } else {
            div += "<a target='_blank' href='" + docs[i].url + "'>";
            div += "<img src='" + docs[i].image + "' height=70 width=50 style = 'border-radius: 5px;'></a></td><td>";
        }
        div += "<table><tr><td class='myTable'>";
        div += "<span align='left' style='display: inline'><b><a target='_blank' href='" + docs[i].url + "'>" + docs[i].title + "</a></b></span>";

        div += "<span style='display:none;float:right' id='" + docs[i].id + "'>";
        div += "<a target='_blank' " +
            "href=\"javascript:fbShare('http://brain-multiplexer.com/redir?q=" + docs[i].id + "', 'Fb Share', 'Facebook share popup', '" + docs[i].image + "', 520, 350)\"><img src='img/fb-share.png'></a>";

        div += " <a target='_blank' " +
            "href=\"javascript:twitterShare('http://brain-multiplexer.com/redir?q=" + docs[i].id + "', 'Twitter Share', 'Twitter share popup', 'http://goo.gl/dS52U', 520, 350)\"><img src='img/twitter-share.png'></a>";


        div += " <i><a href='#' onclick='getSimilarResults($(this).parent().parent().attr(\"id\")," + "\"" + escape(docs[i].title) + "\")'>similar</a></i></span></td>";

        div += "<tr><td><h5 align='left' style='color: #4ea923;display: inline'>" + docs[i].url + "</h5></td></tr>";
        div += "<tr><td><p align='left' style='display: inline'>" + docs[i].paragraph + "</p></td>";
        div += "<tr><td><p align='left' style='display: inline'>Topic: " + docs[i].topic + "</p></td></tr>";
        div += "</table></td></tr></table></li><li>";
    }
    div += "</ul>";

    return div;
}

function fbShare(url, title, descr, image, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open('http://www.facebook.com/sharer.php?s=100&p[title]=' + title + '&p[summary]=' + descr + '&p[url]=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}

function twitterShare(url, title, descr, image, winWidth, winHeight) {
    var winTop = (screen.height / 2) - (winHeight / 2);
    var winLeft = (screen.width / 2) - (winWidth / 2);
    window.open('https://twitter.com/share?url=' + url, 'sharer', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
}

function populateDataList(data) {
    s = data.suggestions;
    var div = "";
    for(i = 0; i < s.length; i++) {
        div += "<option value=\"" + s[i] + "\">";
    }
    $("#languages").html(div);
}

function showSimilar(object) {
    object.style.visibility = "visible";
}

function hideSimilar(object) {
    object.style.visibility = "hidden";
}