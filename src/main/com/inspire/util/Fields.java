package com.inspire.util;

public class Fields {
    public static final String FIELD_PATH = "path";
    public static final String FIELD_TITLE = "title";
    public static final String FIELD_URL = "url";
    public static final String FIELD_DOMAIN = "domain";

    public static final String FIELD_CONTENTS = "contents";
    public static final String FIELD_PARAGRAPH = "paragraph";
    public static final String FIELD_INDEX = "index";
    public static final String FIELD_VIDEO_PROVIDER = "vprovider";
    public static final String FIELD_VIDEO_ID = "vid";
    public static final String FIELD_VIDEO_DURATION = "vlen";
    public static final String FIELD_CATEGORY = "category";
    public static final String FIELD_IMAGE = "image";

    public static final String FIELD_PERSONS = "persons";
    public static final String FIELD_ORGANIZATIONS = "organizations";
    public static final String FIELD_LOCATIONS = "loations";
    public static final String FIELD_MISC = "misc";

    public static final String FIELD_TOPIC_ONE = "t1";
    public static final String FIELD_TOPIC_TWO = "t2";
    public static final String FIELD_TOPIC_THREE = "t3";

    public static final String FIELD_RELATED_BY_FIRST_TOPIC = "rft";
    public static final String FIELD_RELATED_BY_SECOND_TOPIC = "rst";
    public static final String FIELD_RELATED_BY_THIRD_TOPIC = "rtt";

    public static final String FIELD_URL_MD5 = "md5";
    public static final String FIELD_CONTENT_MD5 = "hash";

    public static final String FIELD_SUB_TITLE = "subtitle";
    public static final String FIELD_EPISODE = "episode";
    public static final String FIELD_SERIES = "series";


    public static final String LDA_FIELD_DOC = "doc";
    public static final String LDA_FIELD_SIMILAR_SET = "set";
    public static final String LDA_FIELD_TOPIC = "topic";


}
