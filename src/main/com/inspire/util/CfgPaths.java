package com.inspire.util;

public class CfgPaths {
    private String indexDir;
    private String spellDir;
    private String autocompleteDir;
    private String lda;
    private String db;
    private String stopwords;


    public CfgPaths(String indexDir, String spellDir, String autocompleteDir, String lda, String db, String stopwords) {
        this.indexDir = indexDir;
        this.spellDir = spellDir;
        this.autocompleteDir = autocompleteDir;
        this.lda = lda;
        this.db = db;
        this.stopwords = stopwords;
    }

    public String getIndexDir() {
        return indexDir;
    }

    public String getSpellDir() {
        return spellDir;
    }

    public String getAutocompleteDir() {
        return autocompleteDir;
    }

    public String getLda() {
        return lda;
    }

    public String getDb() {
        return db;
    }

    public String getStopwords() {
        return stopwords;
    }
}
