package com.inspire.util;

public class Constants {


    public static final String REGEX_SERIES = "(?<subtitle>.*?)(?<episode>episode|ep|part|pt)\\s*(?<number>[\\d]+)(.*?)";

    public static final String JSON_RESULTS = "results";
    public static final String JSON_TOTAL = "total";
    public static final String JSON_DOCS = "docs";
    public static final String JSON_TAGS = "tags";
    public static final String JSON_DOCUMENTS = "documents";
    public static final String JSON_FINAL = "final";
    public static final String JSON_IGNORE = "ignore";

    public static final String JSON_TITLE = "title";
    public static final String JSON_URL = "url";
    public static final String JSON_PARAGRAPH = "paragraph";
    public static final String JSON_IMAGE = "image";
    public static final String JSON_ID = "id";
    public static final String JSON_SERIES = "series";
    public static final String JSON_DIDYOUMEAN = "didyoumean";
    public static final String JSON_TOPIC = "topic";

    public static final String SEARH_QUERY = "query";

    public static final String SESSION_INDEX_SEARCHER = "indexSearcher";
    public static final String SESSION_LAST_SEARCH = "lastSearchQuery";
    public static final String SESSION_LIST_DOC_IDS = "jsonArray";

    public static final String SESSION_LAST_INDEX = "lastIndex";

}
