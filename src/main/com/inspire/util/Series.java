package com.inspire.util;

import org.apache.lucene.document.Document;

public class Series implements Comparable {
    private Document doc;
    private int episodeNumber;

    public Series(Document doc, int episodeNumber) {
        this.doc = doc;
        this.episodeNumber = episodeNumber;
    }

    public int getEpisodeNumber() {
        return episodeNumber;
    }

    public Document getDoc() {
        return doc;
    }

    public int compareTo(Object o1) {
        if (o1 instanceof Series) {
            Series series = (Series) o1;
            if (this.episodeNumber == series.getEpisodeNumber())
                return 0;
            else if (this.episodeNumber < series.getEpisodeNumber())
                return -1;
            else
                return 1;
        }
        return -1;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Series) {
            Series series = (Series)o;
            if (this.episodeNumber == series.getEpisodeNumber() && this.doc == series.getDoc()){
                return true;
            }
        }
        return false;
    }
}