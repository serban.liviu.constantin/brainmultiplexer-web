package com.inspire.search;


public class Doc {
    private String id;
    private String url;
    private String title;
    private String image;
    private String paragraph;
    private String topic;
    private String series;

    public Doc(String id, String url, String title, String image, String paragraph) {
        this.id = id;
        this.url = url;
        this.title = title;
        this.image = image;
        this.paragraph = paragraph;
    }

    public String getId() {
        return id;
    }


    public String getParagraph() {
        return paragraph;
    }


    public String getImage() {
        return image;
    }


    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
