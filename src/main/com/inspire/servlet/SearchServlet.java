package com.inspire.servlet;

import com.inspire.search.Doc;
import com.inspire.search.Search;
import com.inspire.session.SearchSession;
import com.inspire.stem.Stemmer;
import com.inspire.util.Constants;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(IndexServlet.class);

    private Search searchInterface;
    public static IndexSearcher ldaIndexSearcher;

    private static final String SEMANTIC_SIMILARITY_STRING = "related topic with:";

    public void init() throws ServletException {
        Stemmer.getInstance().buildEnglishStopWordList();
        searchInterface = Search.getInstance();

        try {
            DirectoryReader ireader = DirectoryReader.open(FSDirectory.open(Paths.get(IndexServlet.cfgPaths.getLda())));
            ldaIndexSearcher = new IndexSearcher(ireader);

        } catch (IOException ioe) {
            throw new ServletException();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = SearchSession.getInstance().getOrSetSearchSession(request);

  //      response.setContentType("application/json");

        String query = request.getParameter("query");
        if (query == null || query.length() == 0)
            return;

        query = StringEscapeUtils.unescapeHtml4(query).replaceAll("[^\\x20-\\x7e]", "");

        if (query.startsWith(SEMANTIC_SIMILARITY_STRING)) {
            return;
        } else {

            IndexSearcher indexSearcher = (IndexSearcher) session.getAttribute(Constants.SESSION_INDEX_SEARCHER);
            if (indexSearcher != null)
                searchIndex(query, indexSearcher, session, request, response);
        }
    }

    private void searchIndex(String searchString, IndexSearcher indexSearcher, HttpSession session,
                             HttpServletRequest request, HttpServletResponse response)
            throws ServletException {

        do {
            try {
                TopDocs topDocs = null;
                JSONObject jsonObject = null;

                List<Doc> results = null;
                topDocs = searchInterface.searchAll(searchString, indexSearcher);
                if (topDocs == null)
                    break;

                session.setAttribute(Constants.SESSION_LAST_SEARCH, searchString);

                request.setAttribute(Constants.JSON_IGNORE, "0");
                //jsonObject.put(Constants.JSON_IGNORE, "0");

                if (topDocs.totalHits == 0) {
                    jsonObject = searchInterface.getSearchSuggestion(searchString);
                    if (jsonObject == null) {
                        request.setAttribute(Constants.JSON_RESULTS, "-1");
                    } else {
                        request.setAttribute(Constants.JSON_RESULTS, "0");
                        request.setAttribute(Constants.JSON_DIDYOUMEAN, jsonObject.getString(Constants.JSON_DIDYOUMEAN));
                    }
                } else {
                    List<ScoreDoc> list = searchInterface.filterResults(topDocs.scoreDocs, indexSearcher);
                    session.setAttribute(Constants.SESSION_LIST_DOC_IDS, list);

                    if (list.size() <= 10) {
                        session.setAttribute(Constants.SESSION_LAST_INDEX, list.size());
                        results = searchInterface.getSearchDocs(list, indexSearcher, 0, list.size());
                    } else {
                        session.setAttribute(Constants.SESSION_LAST_INDEX, 10);
                        results = searchInterface.getSearchDocs(list, indexSearcher, 0, 10);
                    }

                    request.setAttribute(Constants.JSON_RESULTS, "1");
                    //jsonObject.put(Constants.JSON_RESULTS, "1");

                    request.setAttribute(Constants.JSON_TOTAL, list.size());
                    //jsonObject.put(Constants.JSON_TOTAL, list.size());

                    request.setAttribute(Constants.JSON_DOCS, results);

                    request.setAttribute(Constants.SEARH_QUERY, searchString);
                }

                //JSONObject json = new JSONObject();
                //json.put(Constants.JSON_DOCUMENTS, jsonObject);

                request.getRequestDispatcher("/WEB-INF/ftl/search.ftl").forward(request, response);

            } catch (CorruptIndexException cie) {
                LOGGER.fatal("index is corrupt ", cie);
            } catch (IOException ioe) {
                LOGGER.fatal("io excetion caught ", ioe);
            } catch (JSONException je) {
                LOGGER.fatal("json exception caught ", je);
            }
        } while (false);
    }
}
