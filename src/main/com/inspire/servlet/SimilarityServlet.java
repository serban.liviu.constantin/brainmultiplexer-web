package com.inspire.servlet;

import com.inspire.search.Search;
import com.inspire.session.SearchSession;
import com.inspire.util.Constants;
import com.inspire.util.Fields;
import org.apache.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

@WebServlet(name = "SimilarityServlet")
public class SimilarityServlet extends HttpServlet {

    private static final Logger LOGGER = Logger.getLogger(SimilarityServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        do {
            HttpSession session = SearchSession.getInstance().getOrSetSearchSession(request);

            String query = request.getParameter("query");
            if (query == null || query.length() == 0) {
                return;
            }

            query = query.replaceAll("\\\"", "");
            session.setAttribute("lastSearchQuery", query);

            response.setContentType("application/json");

            try {
                IndexSearcher indexSearcher = (IndexSearcher)session.getAttribute("indexSearcher");
                JSONObject jsonObject = getSimilarDocuments(query, indexSearcher);
                if (jsonObject == null)
                    break;

                jsonObject.put("results", "1");
                JSONObject json = new JSONObject();
                json.put("documents", jsonObject);

                response.getWriter().write(json.toString());
            } catch (JSONException j) {
                LOGGER.error("json caught in SimilarityServlet.doGet() ", j);
            } catch (IOException e) {
                LOGGER.error("io exception caught in SimilarityServlet.doGet() ", e);
            }
        } while (false);
    }

    private JSONObject getSimilarDocuments(String hash, IndexSearcher indexSearcher)
            throws IOException, JSONException {

        Map<String, List<String>> videoMap = new HashMap<>();
        JSONObject jsonObject = null;
        do {
            try {
                IndexSearcher latentDirichletAllocation = SearchServlet.ldaIndexSearcher;
                if (latentDirichletAllocation == null) {
                    break;
                }
                Analyzer analyzer = new StandardAnalyzer();
                QueryParser queryParser = new QueryParser(Fields.LDA_FIELD_DOC, analyzer);
                Query query = queryParser.parse(hash);
                TopDocs ldaDocs = latentDirichletAllocation.search(query, 1);

                if (ldaDocs.totalHits == 0)
                    break;

                Document document =  latentDirichletAllocation.doc(ldaDocs.scoreDocs[0].doc);

                String val = document.get(Fields.LDA_FIELD_SIMILAR_SET);

                System.out.println(val);

                List<String> set = new ArrayList<String>();

                val = val.replaceAll("[\\[\\]]", "");
                StringTokenizer st = new StringTokenizer(val, ",");
                while(st.hasMoreTokens())
                    set.add(st.nextToken());

                JSONArray array = new JSONArray();
                for (String sig : set) {
                    TopDocs topDocs = Search.getInstance().searchForOneResult(sig, indexSearcher);
                    if (topDocs == null || topDocs.totalHits != 1) {
                        System.out.println("error: topDoc is null");
                        continue;
                    }
                    int docId = topDocs.scoreDocs[0].doc;
                    Document doc = indexSearcher.doc(docId);
                    Search.getInstance().addDocToJsonArray(doc, array, false, videoMap);
                }

                jsonObject = new JSONObject();
                jsonObject.put("docs", array);

            } catch (ParseException pe) {
                LOGGER.error("parse exception caught in SimilarityServlet.doGet() ", pe);
            }
        } while (false);
        return jsonObject;
    }

    public static Document getDocumentTopic(String hash) {

        Document document = null;

        do {
            try {
                IndexSearcher latentDirichletAllocation = SearchServlet.ldaIndexSearcher;
                if (latentDirichletAllocation == null) {
                    break;
                }
                Analyzer analyzer = new StandardAnalyzer();
                QueryParser queryParser = new QueryParser(Fields.LDA_FIELD_DOC, analyzer);
                Query query = queryParser.parse(hash);
                TopDocs ldaDocs = latentDirichletAllocation.search(query, 1);

                if (ldaDocs.totalHits == 0)
                    break;

               document  = latentDirichletAllocation.doc(ldaDocs.scoreDocs[0].doc);
            } catch (ParseException pe) {
                LOGGER.error("parse exception caught in SimilarityServlet.doGet() ", pe);
            } catch (IOException ioe) {
                LOGGER.error("io exception caught in SimilarityServlet.doGet() ", ioe);
            }
        } while (false);

        return document;
    }
}
