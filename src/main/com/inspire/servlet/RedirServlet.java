package com.inspire.servlet;

import com.inspire.search.Search;
import com.inspire.session.SearchSession;
import com.inspire.util.Constants;
import com.inspire.util.Fields;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Paths;


@WebServlet(name = "RedirServlet")
public class RedirServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        do {

            String q = request.getParameter("q");
            if (q == null || q.isEmpty())
                break;

            DirectoryReader ireader = DirectoryReader.open(FSDirectory.open(Paths.get(IndexServlet.cfgPaths.getIndexDir())));

            IndexSearcher indexSearcher = new IndexSearcher(ireader);

            TopDocs topDocs = Search.getInstance().searchForOneResult(q, indexSearcher);
            if (topDocs == null)
                break;

            Document document = indexSearcher.doc(topDocs.scoreDocs[0].doc);

            String url = document.get(Fields.FIELD_URL);
            String title = document.get(Fields.FIELD_TITLE);
            String image = document.get(Fields.FIELD_IMAGE);
            String contents = document.get(Fields.FIELD_CONTENTS);


            request.setAttribute("url", url);
            request.setAttribute("title", title);
            request.setAttribute("image", image);
            request.setAttribute("contents", contents);

            request.getRequestDispatcher("/WEB-INF/ftl/share.ftl").forward(request, response);


        } while (false);
        return;
    }
}
