package com.inspire.servlet;

import com.inspire.util.CfgPaths;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

public class IndexServlet extends HttpServlet {

    public static CfgPaths cfgPaths;

    public void init(ServletConfig config) throws ServletException {

        String index = config.getInitParameter("indexDir");
        String autocomplete = config.getInitParameter("autocompleteDir");
        String spell = config.getInitParameter("spellDir");
        String lda = config.getInitParameter("ldaDir");
        String db = config.getInitParameter("feedbackDB");
        String stopwords = config.getInitParameter("stopwordsFile");

        index = config.getServletContext().getRealPath(index);
        autocomplete = config.getServletContext().getRealPath(autocomplete);
        spell = config.getServletContext().getRealPath(spell);
        lda = config.getServletContext().getRealPath(lda);
        db = config.getServletContext().getRealPath(db);
        stopwords = config.getServletContext().getRealPath(stopwords);

        cfgPaths = new CfgPaths(index, spell, autocomplete, lda, db, stopwords);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.getRequestDispatcher("/WEB-INF/ftl/search.ftl").forward(request, response);
    }
}
