package com.inspire.tags;

public class Tag {
    private String tag;
    private int occurences;

    public Tag(String tag, int occurences) {
        this.tag = tag;
        this.occurences = occurences;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getOccurences() {
        return occurences;
    }

    public void setOccurences(int occurences) {
        this.occurences = occurences;
    }
}
