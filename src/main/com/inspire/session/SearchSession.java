package com.inspire.session;

import com.inspire.servlet.IndexServlet;
import com.inspire.util.Constants;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class SearchSession {

    private SearchSession() {}

    private static final SearchSession INSTANCE = new SearchSession();

    public static SearchSession getInstance() {
        return INSTANCE;
    }

    public HttpSession getOrSetSearchSession(HttpServletRequest request)
            throws IOException {

        HttpSession session = request.getSession(true);
        Date createTime = new Date(session.getCreationTime());
        Date lastAccessTime = new Date(session.getLastAccessedTime());

        String userIDKey = "userID";
        String userID = "ABCD";

        // Check if this is new comer on your web page.
        if (session.isNew()){

            DirectoryReader ireader = DirectoryReader.open(FSDirectory.open(Paths.get(IndexServlet.cfgPaths.getIndexDir())));

            IndexSearcher indexSearcher = new IndexSearcher(ireader);

            session.setAttribute(userIDKey, userID);

            session.setAttribute(Constants.SESSION_LAST_INDEX, -1);

            session.setAttribute(Constants.SESSION_INDEX_SEARCHER, indexSearcher);
        }

        System.out.println("SessionId: " + session.getId());
        System.out.println("Create Time : " + createTime);
        System.out.println("Last Access Time : " + lastAccessTime);
        System.out.println("Last Search Query : " + session.getAttribute("lastSearchQuery"));

        return session;
    }
}
